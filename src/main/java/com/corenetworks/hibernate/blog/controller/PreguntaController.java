package com.corenetworks.hibernate.blog.controller;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.corenetworks.hibernate.blog.beans.RespuestaBean;
import com.corenetworks.hibernate.blog.beans.PreguntaBean;
import com.corenetworks.hibernate.blog.beans.RegisterBean;
import com.corenetworks.hibernate.blog.dao.RespuestaDao;
import com.corenetworks.hibernate.blog.dao.PreguntaDao;
import com.corenetworks.hibernate.blog.dao.ProfesorDao;
import com.corenetworks.hibernate.blog.model.Respuesta;
import com.corenetworks.hibernate.blog.model.Pregunta;
import com.corenetworks.hibernate.blog.model.Profesor;



@Controller
public class PreguntaController {
	@Autowired
	private PreguntaDao preguntaDao;
	
	@Autowired
	private RespuestaDao respuestaDao;
	
	@Autowired
	private HttpSession httpSession;

	@GetMapping(value = "/submit")
	public String showForm(Model modelo) {
		modelo.addAttribute("pregunta", new PreguntaBean());
		return "submit";
	}
	
	@PostMapping(value = "/submit/newpost")
	public String submit(@ModelAttribute("pregunta") PreguntaBean preguntaBean , Model model) {
		//userDao.create(new User(r.getNombre(), r.getEmail(), r.getCiudad(), r.getPassword()));
         // crear un Post
		 // Obtener el autor desde la sesión
		 // Asignar el autor al post
		 // Hacer persistente el post
		Pregunta pregunta = new Pregunta();
		pregunta.setTitulo(preguntaBean.getTitulo());
		pregunta.setContenido(preguntaBean.getContenido());
		pregunta.setUrl(preguntaBean.getUrl());
		
		Profesor autor = (Profesor) httpSession.getAttribute("userLoggedIn");
		pregunta.setAutor(autor);
		preguntaDao.create(pregunta);
		autor.getPregunta().add(pregunta);
		
		return "redirect:/";
	}	
	
	@GetMapping(value = "/post/{id}")
	public String detail(@PathVariable("id")  long id,   Model modelo) {
		//modelo.addAttribute("post", new PostBean());
		// Comprobar si el Post existe.
		
		Pregunta result = null;
		if ((result = preguntaDao.getById(id)) != null) {
			modelo.addAttribute("pregunta", result);
			modelo.addAttribute("respuestaForm", new RespuestaBean());
			return "postdetail";			
		} else
			return "redirect:/";
	}
	
	@PostMapping(value = "/submit/newComment")
	public String submitRespuesta(@ModelAttribute("respuestaForm") RespuestaBean respuestaBean , Model model) {
		Profesor autor = (Profesor) httpSession.getAttribute("userLoggedIn");
		
		Respuesta respuesta = new Respuesta();
		respuesta.setProfesor(autor);
		
		Pregunta pregunta = preguntaDao.getById(respuestaBean.getPregunta_id());
		respuesta.setPregunta(pregunta);
		respuesta.setContenido(respuestaBean.getContenido());
		respuestaDao.create(respuesta);
		pregunta.getRespuesta().add(respuesta);
		autor.getRespuesta().add(respuesta);
		
		
		return "redirect:/post/"+ respuestaBean.getPregunta_id();
	}	
	
	
}
